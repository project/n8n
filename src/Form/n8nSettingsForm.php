<?php

namespace Drupal\n8n\Form;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Acerta language settings for this site.
 */
class n8nSettingsForm extends ConfigFormBase
{

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a WebformAdminConfigHandlersForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\webform\WebformTokenManagerInterface $token_manager
   *   The webform token manager.
   * @param \Drupal\webform\Plugin\WebformHandlerManagerInterface $handler_manager
   *   The webform handler manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $connection)
  {
    parent::__construct($config_factory);
    $this->moduleHandler = $config_factory;
    $this->database = $connection;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'n8n_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ['n8n.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('n8n.settings');
    $form['global'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('n8n connector config'),
    );
    $enabled = $config->get('enabled');
    $form['global']['n8n_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable n8n triggers'),
      '#default_value' => $enabled,
    ];
    $hostname = $config->get('host');
    $form['global']['n8n_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('n8n URL'),
      '#description' => $this->t('The n8n hostname'),
      '#default_value' => $hostname,
    ];

    $form['users'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('User triggers'),
    );

    $selectedUserEvents = $config->get('user.events');
    $form['users']['user_triggers'] = [
      '#type' => 'checkboxes',
      //'#title' => 'Events',
      '#options' => ['created' => t('Created'), 'updated' => t('Updated'), 'deleted' => t('Deleted')],
      '#default_value' => !empty($selectedUserEvents) ? $selectedUserEvents : [],
      '#required' => FALSE,
      //'#size' => 6,
      '#multiple' => TRUE,
      //'#element_validate' => [[get_class($this), 'elementValidateFilter']],
      //'#ajax' => FALSE,
      '#limit_validation_errors' => [],
    ];

    $form['node'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Node triggers'),
    );

    $selectedNodeEvents = $config->get('node.events');
    $form['node']['node_triggers'] = [
      '#type' => 'checkboxes',
      //'#title' => 'Events',
      '#options' => ['created' => t('Created'), 'updated' => t('Updated'), 'deleted' => t('Deleted')],
      '#default_value' => !empty($selectedNodeEvents) ? $selectedNodeEvents : [],
      '#required' => FALSE,
      //'#size' => 6,
      '#multiple' => TRUE,
      //'#element_validate' => [[get_class($this), 'elementValidateFilter']],
      //'#ajax' => FALSE,
      '#limit_validation_errors' => [],
    ];

    $form['entity'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Entity triggers'),
    );

    $selectedEntityEvents = $config->get('entity.events');
    $form['entity']['entity_triggers'] = [
      '#type' => 'checkboxes',
      //'#title' => 'Events',
      '#options' => ['created' => t('Created'), 'updated' => t('Updated'), 'deleted' => t('Deleted')],
      '#default_value' => !empty($selectedEntityEvents) ? $selectedEntityEvents : [],
      '#required' => FALSE,
      //'#size' => 6,
      '#multiple' => TRUE,
      //'#element_validate' => [[get_class($this), 'elementValidateFilter']],
      //'#ajax' => FALSE,
      '#limit_validation_errors' => [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
//    kint($form_state->getValue('n8n_enabled'));
//    kint($form_state->getValue('n8n_host'));
//    kint($form_state->getValue('entity_triggers'));
//    kint($form_state->getValue('user_triggers'));
//    kint($form_state->getValue('node_triggers'));
    $config = $this->config('n8n.settings');
    $config
      ->set('enabled', $form_state->getValue('n8n_enabled'))
      ->set('host', $form_state->getValue('n8n_host'))
      ->set('user.events', $form_state->getValue('user_triggers'))
      ->set('node.events', $form_state->getValue('node_triggers'))
      ->set('entity.events', $form_state->getValue('entity_triggers'))
      ->save();
    //die();
    parent::submitForm($form, $form_state);
  }
}
