<?php

namespace Drupal\n8n\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Site\Settings;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class EventsController
 * @package Drupal\n8n\Controller
 */
class N8nEvent
{
  public $name;
  public $value;
  public $description;
}

class EventsController
{

  public function __construct()
  {

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static();
  }

  private function eventList()
  {
    $events = [];
    //User
    $userCreated = new n8nEvent();
    $userCreated->name = 'user_created';
    $userCreated->description = 'A user was created';
    $userCreated->value = $userCreated->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $userCreated);
    }

    $userUpdated = new n8nEvent();
    $userUpdated->name = 'user_updated';
    $userUpdated->description = 'A user was updated';
    $userUpdated->value = $userUpdated->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $userUpdated);
    }

    $userDeleted = new n8nEvent();
    $userDeleted->name = 'user_deleted';
    $userDeleted->description = 'A user was deleted';
    $userDeleted->value = $userDeleted->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $userDeleted);
    }

    //Node
    $nodeCreated = new n8nEvent();
    $nodeCreated->name = 'node_created';
    $nodeCreated->description = 'A node was created';
    $nodeCreated->value = $nodeCreated->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $nodeCreated);
    }

    $nodeUpdated = new n8nEvent();
    $nodeUpdated->name = 'node_updated';
    $nodeUpdated->description = 'A node was updated';
    $nodeUpdated->value = $nodeUpdated->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $nodeUpdated);
    }

    $nodeDeleted = new n8nEvent();
    $nodeDeleted->name = 'node_deleted';
    $nodeDeleted->description = 'A node was deleted';
    $nodeDeleted->value = $nodeDeleted->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $nodeDeleted);
    }

    //entity
    $entityCreated = new n8nEvent();
    $entityCreated->name = 'entity_created';
    $entityCreated->description = 'A entity was created';
    $entityCreated->value = $entityCreated->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $entityCreated);
    }

    $entityUpdated = new n8nEvent();
    $entityUpdated->name = 'entity_updated';
    $entityUpdated->description = 'A entity was updated';
    $entityUpdated->value = $entityUpdated->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $entityUpdated);
    }

    $entityDeleted = new n8nEvent();
    $entityDeleted->name = 'entity_deleted';
    $entityDeleted->description = 'A entity was deleted';
    $entityDeleted->value = $entityDeleted->name;
    if ($this->eventIsEnabled($userCreated->name)) {
      array_push($events, $entityDeleted);
    }
return $events;
  }

  // only return enabled events to API HOOK.
  private function eventIsEnabled($eventId)
  {
    return eventIsEnabled($eventId);
  }

  /**
   *
   * @param Request $request
   */
  public function list(Request $request)
  {
    $list = $this->eventList();
    return new JsonResponse($list);
    //return $this->exportPermissions($request, 'xls');
  }


  /**
   *
   * @param Request $request
   */
  public function exportPermissions(Request $request, $suffix)
  {

  }

}
